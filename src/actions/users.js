import axios from 'axios';
import { API_ENDPOINT } from '../config/constants';

export const USERS_LIST = 'USERS_LIST';
export const RESET_FLAG = 'RESET_FLAG';
export const usersList = () => {
  return dispatch => {
    axios.get(`${API_ENDPOINT}/users`).then(response => {
      const { data } = response;
      dispatch({ type: RESET_FLAG });
      dispatch({ type: USERS_LIST, data });
    });
  };
};

export const deleteUser = id => {
  return dispatch => {
    axios.delete(`${API_ENDPOINT}/users/${id}/delete`).then(() => {
      dispatch(usersList());
    });
  };
};

export const UPDATE_USER = 'UPDATE_USER';
export const updateUser = (id, data) => {
  return dispatch => {
    axios.patch(`${API_ENDPOINT}/users/${id}/update`, data).then(() => {
      dispatch({ type: UPDATE_USER, flag: true });
    });
  };
};

export const CREATE_USER = 'CREATE_USER';
export const createUser = data => {
  return dispatch => {
    axios.post(`${API_ENDPOINT}/users`, data).then(() => {
      dispatch({ type: CREATE_USER, flag: true });
    });
  };
};

export const GET_USER = 'GET_USER';
export const getUser = id => {
  return dispatch => {
    axios.get(`${API_ENDPOINT}/users/${id}`).then(response => {
      const { data } = response;
      dispatch({ type: GET_USER, user: data[0] });
    });
  };
};
