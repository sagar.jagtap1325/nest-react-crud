import {
  USERS_LIST,
  CREATE_USER,
  UPDATE_USER,
  GET_USER,
  RESET_FLAG
} from '../actions';

function usersDataReducer(state = { users: [], flag: false }, action) {
  switch (action.type) {
    case USERS_LIST:
      return {
        ...state,
        ...{
          users: action.data
        }
      };

    case CREATE_USER:
      return {
        ...state,
        ...{
          flag: action.flag
        }
      };
    case UPDATE_USER:
      return {
        ...state,
        ...{
          flag: action.flag
        }
      };

    case GET_USER:
      return {
        ...state,
        ...{
          user: action.user
        }
      };
    case RESET_FLAG:
      return {
        ...state,
        ...{
          flag: false,
          user: null
        }
      };
    default:
      return state;
  }
}

export default usersDataReducer;
