import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import PropTypes from 'prop-types';
import SimpleReactValidator from 'simple-react-validator';
import { Link } from 'react-router-dom';
import DateFnsUtils from '@date-io/date-fns';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from '@material-ui/pickers';

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
});

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.validator = new SimpleReactValidator();
    this.state = this.props.user;
  }

  _handleEmailFieldChange = e => {
    this.setState({
      email: e.target.value
    });
  };

  _handleNameFieldChange = e => {
    this.setState({
      name: e.target.value
    });
  };

  _handlePhoneFieldChange = e => {
    this.setState({
      phone: parseInt(e.target.value)
    });
  };

  _handleNationalityFieldChange = e => {
    this.setState({
      nationality: e.target.value
    });
  };

  _handleDobFieldChange = date => {
    this.setState({
      date_of_birth: date
    });
  };

  _handleGenderFieldChange = e => {
    this.setState({
      gender: e.target.value
    });
  };

  _handleAddressFieldChange = e => {
    this.setState({
      address: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      this.props.submit(this.state);
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  render() {
    const { classes } = this.props;
    this.validator.purgeFields();
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            {this.props.title}
          </Typography>
          <form className={classes.form} noValidate onSubmit={this.onSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              autoComplete="name"
              autoFocus
              value={this.state.name}
              onChange={this._handleNameFieldChange}
            />

            {this.validator.message('name', this.state.name, 'required')}

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              value={this.state.email}
              onChange={this._handleEmailFieldChange}
            />

            {this.validator.message(
              'email',
              this.state.email,
              'required|email'
            )}

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="phone"
              label="Phone Number"
              name="phone"
              autoComplete="phone"
              autoFocus
              type="number"
              value={this.state.phone}
              onChange={this._handlePhoneFieldChange}
            />

            {this.validator.message('phone', this.state.phone, 'required')}

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="address"
              label="Address"
              name="address"
              autoComplete="address"
              autoFocus
              value={this.state.address}
              onChange={this._handleAddressFieldChange}
            />

            {this.validator.message('address', this.state.address, 'required')}

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="nationality"
              label="Nationality"
              name="nationality"
              autoComplete="nationality"
              autoFocus
              value={this.state.nationality}
              onChange={this._handleNationalityFieldChange}
            />

            {this.validator.message(
              'nationality',
              this.state.nationality,
              'required'
            )}
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="MM/dd/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Date picker inline"
                value={this.state.date_of_birth}
                onChange={this._handleDobFieldChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date'
                }}
              />
            </MuiPickersUtilsProvider>

            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup
              aria-label="gender"
              name="gender"
              value={this.state.gender}
              onChange={this._handleGenderFieldChange}
            >
              <FormControlLabel
                value="female"
                control={<Radio />}
                label="Female"
              />
              <FormControlLabel value="male" control={<Radio />} label="Male" />
            </RadioGroup>

            <Grid container>
              <Grid item xs>
                <Link to="/">
                  <Button variant="contained" className={classes.submit}>
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Submit
                </Button>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    );
  }
}

UserForm.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

export default withStyles(styles)(UserForm);
