import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Users, AddUser, EditUser } from './pages';

import MiniDrawer from './components/Layout';
function App() {
  return (
    <div className="App">
      <Router>
        <MiniDrawer>
          <Switch>
            <Route exact={true} path="/" component={Users} />
            <Route exact={true} path="/add" component={AddUser} />
            <Route exact={true} path="/:id/edit" component={EditUser} />
          </Switch>
        </MiniDrawer>
      </Router>
    </div>
  );
}

export default App;
