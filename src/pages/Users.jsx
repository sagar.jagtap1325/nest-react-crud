import React from 'react';
import { connect } from 'react-redux';
import { usersList, deleteUser } from '../actions';
import { withStyles } from '@material-ui/core/styles';
import DataTable from '../components/DataTable';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = theme => ({
  search: {
    display: 'flex',
    margin: '10px',
    width: '60%'
  },
  list: { display: 'flex', flexWrap: 'wrap' }
});

class Users extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount = () => {
    this.props.usersList();
  };

  editAction = id => {
    this.props.history.push(`${id}/edit`);
  };

  deleteAction = id => {
    this.props.deleteUser(id);
  };

  render = () => {
    return (
      <div>
        <DataTable
          fields={[
            { field: 'name' },
            { field: 'gender' },
            { field: 'phone', type: 'numeric' },
            { field: 'email' },
            { field: 'address' },
            { field: 'nationality' },
            { field: 'date_of_birth' }
          ]}
          edit={this.editAction}
          delete={this.deleteAction}
          data={this.props.users}
          title={'Users List'}
        >
          <Link to="/add">
            <Button variant="contained" color="primary">
              Add User
            </Button>
          </Link>
        </DataTable>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  users: state.users.users
});

const mapDispatchToProps = {
  usersList,
  deleteUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(Users));
