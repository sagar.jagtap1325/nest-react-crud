import React from 'react';
import { connect } from 'react-redux';
import { updateUser, getUser } from '../actions';
import { withStyles } from '@material-ui/core/styles';
import UserForm from '../components/UserForm';
import { Redirect } from 'react-router-dom';

const useStyles = theme => ({
  search: {
    display: 'flex',
    margin: '10px',
    width: '60%'
  },
  list: { display: 'flex', flexWrap: 'wrap' }
});

class EditUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: false,
      user: {}
    };
  }

  componentDidMount = () => {
    const {
      match: { params }
    } = this.props;
    if (params.id) {
      this.props.getUser(params.id);
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (props.flag !== state.flag) {
      return {
        flag: props.flag,
        user: props.user
      };
    }
    return null;
  }

  submit = data => {
    const {
      match: { params }
    } = this.props;
    this.props.updateUser(params.id, data);
  };

  render = () => {
    return (
      <div>
        {this.state.flag && <Redirect to="/"></Redirect>}
        {this.props.user && (
          <UserForm
            user={this.props.user}
            title={'Edit User'}
            submit={this.submit}
          ></UserForm>
        )}
      </div>
    );
  };
}

const mapStateToProps = state => ({
  flag: state.users.flag,
  user: state.users.user
});
const mapDispatchToProps = {
  updateUser,
  getUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(EditUser));
