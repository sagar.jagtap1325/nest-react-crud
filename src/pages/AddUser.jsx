import React from 'react';
import { connect } from 'react-redux';
import { createUser } from '../actions';
import { withStyles } from '@material-ui/core/styles';
import UserForm from '../components/UserForm';
import { Redirect } from 'react-router-dom';

const useStyles = theme => ({
  search: {
    display: 'flex',
    margin: '10px',
    width: '60%'
  },
  list: { display: 'flex', flexWrap: 'wrap' }
});

class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: '',
        gender: 'male',
        phone: null,
        name: '',
        address: '',
        nationality: '',
        date_of_birth: '2015-01-01T00:00:00.000Z'
      }
    };
  }

  submit = data => {
    this.props.createUser(data);
  };

  render = () => {
    return (
      <div>
        {this.props.flag && <Redirect to="/"></Redirect>}
        {
          <UserForm
            user={this.state.user}
            title={'Add User'}
            submit={this.submit}
          ></UserForm>
        }
      </div>
    );
  };
}

const mapStateToProps = state => ({
  flag: state.users.flag
});
const mapDispatchToProps = {
  createUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(AddUser));
